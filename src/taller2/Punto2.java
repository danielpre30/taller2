/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller2;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import java.util.Iterator;

/**
 *
 * @author Usuario
 */
public class Punto2 {
    public static void main(String[] args){
        
        RandomBag<Integer> bag = new RandomBag<>();
        for(int i=0;i<10;i++){
            int s= StdRandom.uniform(100);
            bag.add(s);
        }

        StdOut.println("Original: ");
        for(Integer d:bag){
            StdOut.print(d+" | ");
        }
           for(int k=0;k<3;k++){
            bag.iteratorRandom();
            StdOut.println("\nAleatorio: ");
            for(Integer i:bag){
                StdOut.print(i+" | ");
            }
        }
        try{
            for(int i=0;i<11;i++){
                StdOut.println("\nSe elimino: "+bag.removeRandom());
            }
        }
        catch(Exception e){
            StdOut.println(e);
        } 
    }
}
