/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller2;

/**
 *
 * @author Usuario
 */
public class Racional {
    private int p,q;
    public Racional(int p, int q){
        this.p=p;
        this.q=q;
    }

    Racional suma(Racional x){
        int num=(this.p*x.q+this.q*x.p);
        int dem=(x.q*this.q);
        return new Racional(num,dem);
    }
    Racional resta(Racional x){
        return new Racional(this.p*x.q-this.q*x.p,this.q*x.q);
    }
    Racional multiplicacion(Racional x){
        return new Racional(this.p*x.p,this.q*x.q);
    }
    Racional division(Racional x){
        return new Racional (this.p*x.q,this.q*x.p);
    }
    @Override
    public String toString(){
    return p+"/"+q;
    }
    static Racional parseRacional(String s){
        int pos = s.indexOf("r");
        int ele=s.indexOf("l");
        int num= Integer.parseInt(s.substring(ele+1,pos));
        int dem = Integer.parseInt(s.substring(pos+1, s.length()));
        return new Racional(num,dem);
    }
}
