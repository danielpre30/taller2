/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller2;
import edu.princeton.cs.algs4.StdOut;
import java.util.Iterator;
import java.util.Random;
import java.util.Collections;
import edu.princeton.cs.algs4.StdRandom;
/**
 *
 * @author Usuario
 * @param <Item>
 */
public class RandomBag<Item> implements Iterable<Item> {
    private Node first;

    
    private int n;
    private class Node{
        Item item;
        Node next;
    }
    
    public void add(Item item){// same as push() in Stack
        Node oldfirst = first;
        first = new Node();
        first.item = item;
        first.next = oldfirst;
        n++;
    }

    public Item removeRandom() {
        Node current = first;
        Node prev = first;
        if(current==null){
            throw new NullPointerException("Cola Vacía");
        }
        int index = new Random().nextInt(n);

//        StdOut.println("El current es:"+current.item);
//        StdOut.println("El index es:"+index);
        
        if(index==0){
            prev=first;
            first=first.next;
            n--;
            return prev.item;
        }
        else{
            for (int i = 0; i < index; i++) {
                prev = current;
                current = current.next;
            } 
            prev.next = current.next;
            current.next = null;
            n--;
        }
        
        return current.item;
    }
           public RandomBag iteratorRandom() {
            RandomBag<Item> newBag = new RandomBag<>();
            Item[] prevBag = (Item[]) new Object[n];
            int k =n;
            
            for(int i=0;i<k;i++){
                Item p=RandomBag.this.removeRandom();
                newBag.add(p);
                prevBag[i]=p;
            }
            for(int i=0;i<k;i++){
                RandomBag.this.add(prevBag[n]);
            }
            return newBag;
    }
    
    @Override
    public Iterator<Item> iterator(){
        return new ListIterator();
    }
    
    private class ListIterator implements Iterator<Item>{
        private Node current = first;
        private Node prev = first;
        @Override
        public boolean hasNext(){
            return current != null;
        }
        
 

        @Override
        public Item next(){
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

}
