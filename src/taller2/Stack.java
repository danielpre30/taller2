/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller2;

/**
 *
 * @author Usuario
 * @param <Item>
 */
public class Stack<Item> {
    private Nodo first;
    private int n;

    private class Nodo {
            Item item;
            Nodo next;
    }

    public void push(Item s) {
            Nodo anterior = first;
            first = new Nodo();
            first.item = s;
            first.next = anterior;   //se agrega al inicio de la lista
            n++;
    }

    public Item pop() /*throws Exception*/ {
//            if (first==null) {
//                    throw new Exception("La pila esta vacia");
//            }
            Item item = first.item;  //item q se retornará
            first = first.next;    //se elimina el first
            n--;
            return item;
    }
    
    public Item peek(){
        
        return first.item;  //item q se retornará
    }

    public boolean isEmpty() {
            return n==0;
    }

    public int size() {
            return n;
    }
    
}
